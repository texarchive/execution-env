FROM registry.gitlab.com/texarchive/tex-compile-env:main
RUN pacman -Sy \
    && pacman -S --noconfirm \
       python \
       python-pip
